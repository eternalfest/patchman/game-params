# 0.11.1 (2021-08-16)

- **[Fix]** Fix incorrect lookup of lives.toturial instead of lives.tutorial.
- **[Fix]** Fix some error messages on incorrect configuration file.

# 0.11.0 (2021-08-07)

- **[Breaking change]** Remove `levels.boss`, `darknessAuto` & `darkness` configuration options.
- The Atlas properties `boss` and `darkness` should now be used instead.
- **[Change]** Use `Var` patches to modify static variables in `hf.Data`.

# 0.10.0 (2021-04-21)

- **[Breaking change]** Update to `patchman@0.10.3`.
- **[Internal]** Update to Yarn 2.

# 0.9.0 (2021-02-19)

- **[Fix]** Incorrect boss level in dimensions.

# 0.8.0 (2021-01-11)

- **[Feature]** Apply darkness.

# 0.7.0 (2021-01-03)

- **[Fix]** Correct number of bombs when taking/losing lamps.

# 0.6.0 (2020-12-18)

- **[Feature]** Apply darknessAuto.

# 0.5.0 (2020-09-05)

- **[Fix]** Correct spawn position of players in mirror multi.

# 0.4.0 (2020-09-05)

- **[Fix]** Compatibility with patchman 0.9.2.

# 0.3.0 (2020-07-19)

- **[Feature]** Remove dependency to @patchman/keyboard.

# 0.2.0 (2020-07-19)

- **[Fix]** Apply various code review suggestions.

# 0.1.0 (2020-07-08)

- **[Feature]** First release.

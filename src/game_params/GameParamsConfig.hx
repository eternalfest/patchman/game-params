package game_params;

import etwin.Obfu;
import patchman.DebugConsole;
import patchman.module.Data;

@:build(patchman.Build.di())
class GameParamsConfig {
    public var adventureLives(default, null): Int;
    public var tutorialLives(default, null): Int;
    public var timeattackLives(default, null): Int;

    public var playerBombs(default, null): Int;
    public var playerHead(default, null): Int;
    public var playerSkin(default, null): Int;

    public var levelBat(default, null): Int;
    public var levelTubz(default, null): Int;
    public var levelSecret(default, null): Int;

    public var spawnIgor(default, null): Array<Int>;
    public var spawnSandy(default, null): Array<Int>;

    public var scoreBonus(default, null): Array<Int>;

    public function new(adventureLives: Int, tutorialLives: Int, timeattackLives: Int, playerBombs: Int, playerHead: Int, playerSkin: Int, levelBat: Int, levelTubz: Int, levelSecret: Int, spawnIgor: Array<Int>, spawnSandy: Array<Int>, scoreBonus: Array<Int>): Void {
        this.adventureLives = adventureLives;
        this.tutorialLives = tutorialLives;
        this.timeattackLives = timeattackLives;
        this.playerBombs = playerBombs;
        this.playerHead = playerHead;
        this.playerSkin = playerSkin;
        this.levelBat = levelBat;
        this.levelTubz = levelTubz;
        this.levelSecret = levelSecret;
        this.spawnIgor = spawnIgor;
        this.spawnSandy = spawnSandy;
        this.scoreBonus = scoreBonus;
    }

    public static var DEFAULT: GameParamsConfig = new GameParamsConfig(6, 2, 0, 2, 1, 1, 100, 101, 10, [5, -2], [14, -2], [100000, 500000, 1000000, 2000000, 3000000, 4000000]);

    private static inline var GAME_PARAMS_CONFIG: String = Obfu.raw("GameParams");

    @:diFactory
    public static function fromHml(dataMod: Data): GameParamsConfig {
        if (!dataMod.has(GAME_PARAMS_CONFIG)) {
            DebugConsole.error(GAME_PARAMS_CONFIG + ".json is missing from the data folder.");
            return GameParamsConfig.DEFAULT;
        }

        var data: Dynamic = dataMod.get(GAME_PARAMS_CONFIG);

        var player: Dynamic = Obfu.field(data, "player");
        if (player == null) {
            return errorMissingField(Obfu.raw("player"), true);
        }

        var playerLives: Dynamic = Obfu.field(player, "lives");
        if (playerLives == null) {
            return errorMissingField("player.lives", true);
        }

        var adventureLives: Int = Obfu.field(playerLives, "adventure");
        if (adventureLives == null) {
            return errorMissingField("player.lives.adventure");
        }

        var tutorialLives: Int = Obfu.field(playerLives, "tutorial");
        if (tutorialLives == null) {
            return errorMissingField("player.lives.tutorial");
        }

        var timeattackLives: Int = Obfu.field(playerLives, "timeAttack");
        if (timeattackLives == null) {
            return errorMissingField("player.lives.timeAttack");
        }

        var playerBombs: Int = Obfu.field(player, "maxBombs");
        if (playerBombs == null) {
            return errorMissingField("player.maxBombs");
        }

        var playerHead: Int = Obfu.field(player, "head");
        if (playerHead == null) {
            return errorMissingField("player.head");
        }

        var playerSkin: Int = Obfu.field(player, "skin");
        if (playerSkin == null) {
            return errorMissingField("player.skin");
        }

        var levels: Dynamic = Obfu.field(data, "levels");
        if (levels == null) {
            return errorMissingField(Obfu.raw("levels"), true);
        }

        if (Obfu.hasField(levels, "boss")) {
            warnRemovedAtlas("levels.boss");
        }

        var levelsBat: Int = Obfu.field(levels, "bat");
        if (levelsBat == null) {
            return errorMissingField("levels.bat");
        }

        var levelsTubz: Int = Obfu.field(levels, "tubz");
        if (levelsTubz == null) {
            return errorMissingField("levels.tubz");
        }

        var levelsSecret: Int = Obfu.field(levels, "secret");
        if (levelsSecret == null) {
            return errorMissingField("levels.secret");
        }

        var scoreBonus: Array<Int> = Obfu.field(data, "scoreBonus");
        if (scoreBonus == null) {
            return errorMissingField(Obfu.raw("scoreBonus"));
        }

        var multiSpawn: Dynamic = Obfu.field(data, "multispawn");
        if (multiSpawn == null) {
            return errorMissingField(Obfu.raw("multispawn"), true);
        }

        var spawnIgor: Array<Int> = Obfu.field(multiSpawn, "igor");
        if (spawnIgor == null)
            spawnIgor = [5, -2];
        var spawnSandy: Array<Int> = Obfu.field(multiSpawn, "sandy");
        if (spawnSandy == null)
            spawnSandy = [14, -2];

        if (Obfu.hasField(data, "darknessAuto")) {
            warnRemovedAtlas(Obfu.raw("darknessAuto"));
        }
        
        if (Obfu.hasField(data, "darkness")) {
            warnRemovedAtlas(Obfu.raw("darkness"));
        }

        return new GameParamsConfig(adventureLives, tutorialLives, timeattackLives, playerBombs, playerHead, playerSkin, levelsBat, levelsTubz, levelsSecret, spawnIgor, spawnSandy, scoreBonus);
    }

    private static function errorMissingField(fieldName: String, ?isObj = false): GameParamsConfig {
        var value = isObj ? "configuration object" : Obfu.raw("value");
        DebugConsole.error('\'$fieldName\' $value is missing from $GAME_PARAMS_CONFIG.json.');
        return GameParamsConfig.DEFAULT;
    }

    private static function warnRemovedAtlas(fieldName: String): Void {
        DebugConsole.warn('\'$fieldName\' has been removed; use the corresponding Atlas property instead.');
    }
}

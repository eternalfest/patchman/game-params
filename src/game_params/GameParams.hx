package game_params;

import etwin.Error;
import etwin.ds.Set;
import etwin.flash.Key;
import hf.Hf;
import hf.entity.Player;
import hf.mode.GameMode;
import hf.mode.Adventure;
import hf.mode.MultiCoop;
import hf.mode.Shareware;
import hf.mode.TimeAttack;
import patchman.IPatch;
import patchman.Ref;
import patchman.PatchList;

@:build(patchman.Build.di())
class GameParams {
    @:diExport
    public var changePlayerData(default, null): IPatch;
    @:diExport
    public var allowAccessToNewHead(default, null): IPatch;
    @:diExport
    public var changeSharewareLives(default, null): IPatch;
    @:diExport
    public var changeTimeattackLives(default, null): IPatch;
    @:diExport
    public var changeGameData(default, null): IPatch;
    @:diExport
    public var changeWellSecretDestination(default, null): IPatch;
    @:diExport
    public var changeMultiSpawn(default, null): IPatch;

    private static function setPlayersLives(game: GameMode, lives: Int): Void {
        for (player in game.getPlayerList()) {
            player.lives = lives;
            game.gi.setLives(player.pid, lives);
        }
    }

    public function new(config: GameParamsConfig): Void {
        // TODO: darkness and nojutsu were based on Zones.

        this.changePlayerData = Ref.auto(Player.initPlayer).before(function(hf: Hf, self: Player, g: GameMode, x: Float, y: Float): Void {
            self.lives = config.adventureLives;
            self.initialMaxBombs = config.playerBombs;
            self.maxBombs = config.playerBombs;
            self.head = config.playerHead;
            self.skin = config.playerSkin;
        });

        this.allowAccessToNewHead = Ref.auto(GameMode.getControls).wrap(function(hf: Hf, self: GameMode, old: GameMode -> Void): Void {
            if (self.fl_disguise && Key.isDown(68) && self.keyLock != 68) {
                var configHead = config.playerHead;
                var accessible: Bool = configHead == 1 || configHead <= 6 && hf.GameManager.CONFIG.hasFamily(108 + configHead);

                if (accessible) {
                    old(self);
                }
                else {
                    var player = self.getPlayerList()[0];
                    var previousHead = player.head;
                    old(self);
                    /* It rolled over, so add the non accessible as the last before roll over. */                    
                    if (player.head == 1 && previousHead != configHead) {
                        player.head = configHead;
                        player.replayAnim();
                    }
                }
            }
            else {
                old(self);
            }
        });

        this.changeSharewareLives = Ref.auto(Shareware.initGame).after(function(hf: Hf, self: Shareware): Void {
            setPlayersLives(self, config.tutorialLives);
        });

        this.changeTimeattackLives = Ref.auto(TimeAttack.initializePlayers).after(function(hf: Hf, self: TimeAttack): Void {
            setPlayersLives(self, config.timeattackLives);
        });

        this.changeGameData = new PatchList([
            Ref.auto(hf.Data.BAT_LEVEL).replace(config.levelBat),
            Ref.auto(hf.Data.TUBERCULOZ_LEVEL).replace(config.levelTubz),
            Ref.auto(hf.Data.EXTRA_LIFE_STEPS).replace(config.scoreBonus),
        ]);

        this.changeWellSecretDestination = Ref.auto(Adventure.nextLevel).replace(function(hf: Hf, self: Adventure): Void {
            Ref.call(hf, super.nextLevel, self);
            if (self.fl_warpStart) {
                self.world.currentId = 0;
                self.unlock();
                self.world.view.detach();
                self.forcedGoto(config.levelSecret);
            }
        });

        this.changeMultiSpawn = Ref.auto(MultiCoop.initGame).after(function(hf: Hf, self: MultiCoop): Void {
            var players: Array<Player> = self.getPlayerList();
            players[0].moveToCase(self.flipCoordCase(config.spawnIgor[0]), config.spawnIgor[1]);
            players[1].moveToCase(self.flipCoordCase(config.spawnSandy[0]), config.spawnSandy[1]);
        });
    }
}
